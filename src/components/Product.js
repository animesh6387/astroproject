import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Breadcrumb, Form, Col, Button } from 'react-bootstrap'
import Navbar from '../components/Navbar';
import Footer from '../components/Footer';
import '../scss/services.scss';
import landr1 from '../images/services/landr1.png';
import landr2 from '../images/services/landr2.jpg';
import landr3 from '../images/services/landr3.jpg';
import landr4 from '../images/services/landr4.jpg';
import landr5 from '../images/services/landr5.jpg';
import landr6 from '../images/services/landr6.jpg';
import landr7 from '../images/services/landr7.jpg';
function Product(props) {

  const [serviceproduct, setServiceProduct] = useState([])
  useEffect(() => {
    axios.get('http://localhost:8080/api/serviceCategoryDetail/' + props.match.params.id)

      .then(res => {
        console.log(res.data);
        setServiceProduct(res.data.data);
      })
      .catch(err => {
        console.log(err);
      })
  })

  const [servicecatgory, setServicecatgory] = useState([])

  useEffect(() => {
    axios.get('http://localhost:8080/api/service')
      .then(res => {
        console.log(res);
        setServicecatgory(res.data.data)
        console.log((res.data))
      })
      .catch(err => {
        console.log(err);
      })
  }, [])

  const [contact, setContact] = useState({
    email: "",
    mobileNumber: "",
    address: "",
    question: ""
  })
  function submitContact(e) {
    e.preventDefault();
    console.log(e)
    axios.post("http://localhost:8080/api/contact", {
      "email": contact.email,
      "mobileNumber": contact.mobileNumber,
      "address": contact.address,
      "question": contact.question
    }
    )
      .then(res => {
        console.log(res.data)
        alert("Successfully inserted");
      })
  }

  function handleContact(e) {
    const newData = { ...contact }
    newData[e.target.id] = e.target.value
    setContact(newData)
    console.log(newData)
  }

  const [birth, setBirth] = useState({
    name: "",
    gender: "",
    maritalStatus: "",
    date: "",
    time: "",
    birthCountry: "",
    birthState: "",
    birthPlace: "",
    logitude: "",
    latitude: ""
  })
  function submit(e) {
    e.preventDefault();
    console.log(e)
    axios.post("http://localhost:8080/api/birth", {
      "name": birth.name,
      "gender": birth.gender,
      "maritalStatus": birth.maritalStatus,
      "date": birth.date,
      "time": birth.time,
      "birthCountry": birth.birthCountry,
      "birthState": birth.birthState,
      "birthPlace": birth.birthPlace,
      "logitude": birth.logitude,
      "latitude": birth.latitude
    }
    )
      .then(res => {
        console.log(res.data)
        alert("Successfully inserted");
      })
  }

  function handle(e) {
    const newData = { ...birth }
    newData[e.target.id] = e.target.value
    setBirth(newData)
    console.log(newData)
  }
  return (
    <>
      <Navbar />
      <div className="header">
        <Breadcrumb className="bread">
          <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
          <Breadcrumb.Item active style={{ color: 'white' }}>{serviceproduct.title} </Breadcrumb.Item>
        </Breadcrumb>
      </div>
      <div className="productrow">
        <div className="landrcolumn3">


          <div className="product1" style={{ padding: '10%', overflow: 'scroll' }}>
            <h4>CATEGORIES</h4>
            <hr></hr>
            {servicecatgory.map(category => {
              return (
                <a href={"/loveandrelationship/" + category.id}><p>{category.title}</p></a>
              );
            })}
          </div>

          <div className="product2" style={{ padding: '10%', marginTop: '10%' }}>
            <h4>BEST SELLERS</h4>
            <hr></hr>
            <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
              <img src={landr1} alt="landr1" style={{ width: 100, height: 100, borderRadius: '50%', marginRight: '10px' }} />
              <p>Free Astrology<br /> Consultation</p>
            </div>
            <hr />
            <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
              <img src={landr2} alt="landr1" style={{ width: 100, height: 100, borderRadius: '50%', marginRight: '10px' }} />
              <p>Free Gemstone<br /> Recommendation</p>
            </div>
            <hr />
            <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
              <img src={landr3} alt="landr1" style={{ width: 100, height: 100, borderRadius: '50%', marginRight: '10px' }} />
              <p>Astrology<br /> Consultancy</p>
            </div>

          </div>
        </div>
        <div className="landrcolumn4">
          <div className="product_wrapper">
            <div className="product_img">
              <img src={landr5} alt="landr5" style={{ marginRight: '20px', borderRadius: '50%' }} />
            </div>
            <div className="product_title">
              <h4 className="Product_heading">{serviceproduct.title}</h4>
              <p className="Product_money">₹{serviceproduct.amount}</p>
              <p className="product_content"><h6 className="Product_heading">Tags: </h6>{serviceproduct.tags}</p>
            </div>
          </div>
          <h4 className="Product_heading">{serviceproduct.title}</h4>
          <p className="product_content">{serviceproduct.description}</p>
          <div className="birth_form" style={{ marginTop: '5%' }}>
            <h3 className="Product_heading">BIRTH INFORMATION</h3>
            <Form onSubmit={(e) => submit(e)}>
              <Form.Row>
                <Form.Group as={Col} controlId="formGridEmail">
                  <Form.Control type="text" placeholder="Name" id="name" required value={birth.name} onChange={(e) => handle(e)} />
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlSelect1">
                  <Form.Control as="select" id="gender" required value={birth.gender} onChange={(e) => handle(e)}>
                    <option>Gender</option>
                    <option>Male</option>
                    <option>Female</option>
                    <option>Transsexual</option>
                  </Form.Control>
                </Form.Group>
                <Form.Group as={Col} controlId="formGridPassword">
                  <Form.Control as="select" id="maritalStatus" required value={birth.maritalStatus} onChange={(e) => handle(e)}>
                    <option>Marital status</option>
                    <option>Married</option>
                    <option>Bachelor</option>
                    <option>Widow/Widower</option>
                    <option>Divorced</option>
                    <option>Live in</option>
                  </Form.Control>
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col} controlId="formGridEmail">
                  <Form.Control type="date" placeholder="Date of Birth" id="date" required value={birth.date} onChange={(e) => handle(e)} />
                </Form.Group>

                <Form.Group as={Col} controlId="formGridPassword">
                  <Form.Control type="time" placeholder="Time of Birth" id="time" required value={birth.time} onChange={(e) => handle(e)} />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col} controlId="formGridEmail">
                  <Form.Control type="text" placeholder="Country Of Birth" id="birthCountry" required value={birth.birthCountry} onChange={(e) => handle(e)} />
                </Form.Group>

                <Form.Group as={Col} controlId="formGridPassword">
                  <Form.Control type="text" placeholder="State Of Birth" id="birthState" required value={birth.birthState} onChange={(e) => handle(e)} />
                </Form.Group>
                <Form.Group as={Col} controlId="formGridPassword">
                  <Form.Control type="text" placeholder="Place Of Birth" id="birthPlace" required value={birth.birthPlace} onChange={(e) => handle(e)} />
                </Form.Group>
              </Form.Row>

              <Form.Row>
                <Form.Group as={Col} controlId="formGridEmail">
                  <Form.Control type="text" placeholder="Longitude" id="logitude" required value={birth.logitude} onChange={(e) => handle(e)} />
                </Form.Group>
                <Form.Group as={Col} controlId="formGridEmail">
                  <Form.Control as="select">
                    <option>East</option>
                    <option>West</option>
                  </Form.Control>
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col} controlId="formGridPassword">
                  <Form.Control type="text" placeholder="Latitude" id="latitude" required value={birth.latitude} onChange={(e) => handle(e)} />
                </Form.Group>
                <Form.Group as={Col} controlId="formGridEmail">
                  <Form.Control as="select">
                    <option>North</option>
                    <option>South</option>
                  </Form.Control>
                </Form.Group>
              </Form.Row>

              <Button variant="primary" type="submit" onSubmit={(e) => submit(e)}>
                Submit
              </Button>
            </Form>

          </div>
          <div className="contact_detail" style={{ marginTop: '5%', marginBottom: '5%' }}>
            <h3 className="Product_heading">CONTACT DETAILS</h3>
            <Form onSubmit={(e) => submitContact(e)}>
              <Form.Row>
                <Form.Group as={Col} controlId="formGridEmail">
                  <Form.Label style={{ color: 'purple' }}>Email address</Form.Label>
                  <Form.Control type="email" placeholder="Email" id="email" required value={contact.email} onChange={(e) => handleContact(e)} />
                </Form.Group>
                <Form.Group as={Col} controlId="formGridEmail">
                  <Form.Label style={{ color: 'purple' }}>Phone/Mobile No</Form.Label>
                  <Form.Control type="number" placeholder="Phone/Mobile No" id="mobileNumber" required value={contact.mobileNumber} onChange={(e) => handleContact(e)} />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col} controlId="formGridEmail">
                  <Form.Label style={{ color: 'purple' }}>Address</Form.Label>
                  <Form.Control as="textarea" rows={3} id="address" required value={contact.address} onChange={(e) => handleContact(e)} />
                </Form.Group>
                <Form.Group as={Col} controlId="formGridEmail">
                  <Form.Label style={{ color: 'purple' }}>Your Question</Form.Label>
                  <Form.Control as="textarea" rows={3} id="question" required value={contact.question} onChange={(e) => handleContact(e)} />
                </Form.Group>
              </Form.Row>
              <Button variant="primary" type="submit" onSubmit={(e) => submitContact(e)}>
                Submit
              </Button>
            </Form>
          </div>
        </div>


      </div>
      <Footer />
    </>
  );
}

export default Product;
