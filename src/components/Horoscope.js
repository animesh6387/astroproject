import React from "react";
import '../scss/App.scss';
import '../scss/font.scss';
import divider from '../images/Line 61.png';
import Slider from "react-slick";
import horoscope1 from '../images/aries.png';
import horoscope2 from '../images/taurus.png';
import horoscope3 from '../images/gemini.png';
import horoscope4 from '../images/cancer.png';
import horoscope5 from '../images/lio.png';
import horoscope6 from '../images/virgo.png';
import horoscope7 from '../images/libra.png';
import horoscope8 from '../images/scorpio.png';
import horoscope9 from '../images/sagittarius.png';
import horoscope10 from '../images/capricom.png';
import horoscope11 from '../images/Aquaries.png';
import horoscope12 from '../images/pisces.png';

function Horoscope() {
  var settings = {
    dots: false,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: false
        }
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true,
          dots: false
        }
      }

    ]
  };
  return (
    <div className="Horoscope">
      <div className="our_services">
        <p className="our_services_text" style={{ marginTop: '10%', }}>HOROSCOPES</p>
        <img className="divider" src={divider} alt="divider" />
      </div>
      <p className="text">Today, what does your zodiac sign recommends? Find out! you can also look into weekly, monthly or yearly detail predictions.</p>
      <p className="text1">  Our expert’s prediction will help you to plan for near future & future.</p>
      <div className="horoscope_carousel">
        <Slider {...settings} className="slider">

          <a href="/Aries"> <div className="horoscopeWrapper">
            <img className="horoscope_image" src={horoscope1} alt="horoscope" />
            <p className="horoscope_text" >Aries</p>
          </div></a>

          <a href="/Taurus"> <div className="horoscopeWrapper">
            <img className="horoscope_image" src={horoscope2} alt="horoscope" />
            <p className="horoscope_text" >Taurus</p>
          </div></a>

          <a href="/Gemini"> <div className="horoscopeWrapper">
            <img className="horoscope_image" src={horoscope3} alt="horoscope" />
            <p className="horoscope_text" >Gemini</p>
          </div></a>

          <a href="/Cancer"><div className="horoscopeWrapper">
            <img className="horoscope_image" src={horoscope4} alt="horoscope" />
            <p className="horoscope_text" >Cancer</p>
          </div></a>

          <a href="/Leo"> <div className="horoscopeWrapper">
            <img className="horoscope_image" src={horoscope5} alt="horoscope" />
            <p className="horoscope_text" >Leo</p>
          </div></a>

          <a href="/Virgo"><div className="horoscopeWrapper">
            <img className="horoscope_image" src={horoscope6} alt="horoscope" />
            <p className="horoscope_text" >Virgo</p>
          </div></a>

          <a href="/Libra"> <div className="horoscopeWrapper">
            <img className="horoscope_image" src={horoscope7} alt="horoscope" />
            <p className="horoscope_text" >Libra</p>
          </div></a>

          <a href="/Scorpio"> <div className="horoscopeWrapper">
            <img className="horoscope_image" src={horoscope8} alt="horoscope" />
            <p className="horoscope_text" >Scorpio</p>
          </div></a>

          <a href="/Sagittarius"> <div className="horoscopeWrapper">
            <img className="horoscope_image" src={horoscope9} alt="horoscope" />
            <p className="horoscope_text" >Sagittarius</p>
          </div></a>

          <a href="/Capricorn"> <div className="horoscopeWrapper">
            <img className="horoscope_image" src={horoscope10} alt="horoscope" />
            <p className="horoscope_text" >Capricorn</p>
          </div></a>

          <a href="/Aquarius"> <div className="horoscopeWrapper">
            <img className="horoscope_image" src={horoscope11} alt="horoscope" />
            <p className="horoscope_text" >Aquarius</p>
          </div></a>

          <a href="/Pisces"> <div className="horoscopeWrapper">
            <img className="horoscope_image" src={horoscope12} alt="horoscope" />
            <p className="horoscope_text" >Pisces</p>
          </div></a>

        </Slider>

      </div>

    </div>
  );
}

export default Horoscope;
