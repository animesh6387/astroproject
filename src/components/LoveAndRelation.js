import React, { useState, useEffect } from "react";
import axios from 'axios';
import { Breadcrumb } from 'react-bootstrap'
import Navbar from '../components/Navbar';
import Footer from '../components/Footer';
import '../scss/services.scss';
import landr1 from '../images/services/landr1.png';
import landr2 from '../images/services/landr2.jpg';
import landr3 from '../images/services/landr3.jpg';
import landr4 from '../images/services/landr4.jpg';
import landr5 from '../images/services/landr5.jpg';
import landr6 from '../images/services/landr6.jpg';
import landr7 from '../images/services/landr7.jpg';
function OurServices(props) {
    const [service, setService] = useState([])
    useEffect(() => {
        axios.get('http://localhost:8080/api/service/' + props.match.params.id)

            .then(res => {
                console.log(res.data);
                setService(res.data.data.servicecategories);
            })
            .catch(err => {
                console.log(err);
            })
    })
    const [servicecatgory, setServicecatgory] = useState([])

    useEffect(() => {
        axios.get('http://localhost:8080/api/service')
            .then(res => {
                console.log(res);
                setServicecatgory(res.data.data)
                console.log((res.data))
            })
            .catch(err => {
                console.log(err);
            })
    }, [])
    return (
        <>
            <Navbar />
            <div>
                {service.map(services => {
                    return (
                        <div>
                            <div className="header">
                                <Breadcrumb className="bread">
                                    <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
                                    <Breadcrumb.Item active style={{ color: 'white' }}>{services.title} </Breadcrumb.Item>
                                </Breadcrumb>
                            </div>
                            <div className="landrrow">

                                <div className="landrcolumn" style={{ overflow: 'scroll' }}>
                                    <h4>CATEGORIES</h4>
                                    <hr></hr>
                                    {servicecatgory.map(category => {
                                        return (
                                            <a href={"/loveandrelationship/" + category.id}><p>{category.title}</p></a>
                                        );
                                    })}
                                </div>

                                <div className="landrcolumn1">
                                    <a href={"/product/" + services.id} > <div class="card mb-3" style={{ maxWidth: '540px', textAlign: 'center', alignContent: 'center', padding: '3%', boxShadow: 'inset 0 0 10px blue' }}>
                                        <div class="row g-0">
                                            <div class="col-md-4">
                                                <img src={services.image} alt="landr4" style={{ width: 150, height: 150, borderRadius: '50%' }} />
                                            </div>
                                            <div class="col-md-8" style={{ alignSelf: 'center' }}>
                                                <div class="card-body">
                                                    <h5 class="card-title" style={{ color: 'black' }}>{services.title}</h5>
                                                    <p class="card-text" style={{ color: 'black' }}>{services.amount}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </a>

                                </div>
                                <div className="landrcolumn2">
                                    <h4>BEST SELLERS</h4>
                                    <hr></hr>
                                    <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                                        <img src={landr1} alt="landr1" style={{ width: 100, height: 100, borderRadius: '50%', marginRight: '10px' }} />
                                        <p>Free Astrology<br /> Consultation</p>
                                    </div>
                                    <hr />
                                    <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                                        <img src={landr2} alt="landr1" style={{ width: 100, height: 100, borderRadius: '50%', marginRight: '10px' }} />
                                        <p>Free Gemstone<br /> Recommendation</p>
                                    </div>
                                    <hr />
                                    <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                                        <img src={landr3} alt="landr1" style={{ width: 100, height: 100, borderRadius: '50%', marginRight: '10px' }} />
                                        <p>Astrology<br /> Consultancy</p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    );
                })}
            </div>
            <Footer />
        </>
    );
}

export default OurServices;
