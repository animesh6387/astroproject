import React, { useState, useEffect } from "react";
import axios from 'axios';
import '../scss/App.scss';
import '../scss/font.scss';
import search from '../images/search.png'
import contact from '../images/user.png'
import location from '../images/pin.png'
import logo from '../images/logo1.png';
import { Navbar, Nav, NavDropdown, Form, Dropdown } from 'react-bootstrap';


function NavBar() {
  const [servicecatgory, setServicecatgory] = useState([])
  const [product, setProduct] = useState([])

  useEffect(() => {
    axios.get('http://localhost:8080/api/service')
      .then(res => {
        console.log(res);
        setServicecatgory(res.data.data)
        console.log((res.data))
      })
      .catch(err => {
        console.log(err);
      })
  }, [])

  useEffect(() => {
    axios.get('http://localhost:8080/api/product')
      .then(res => {
        console.log(res);
        setProduct(res.data.data)
        console.log((res.data))
      })
      .catch(err => {
        console.log(err);
      })
  }, [])


  return (
    // <div className="Navbar">
    //     <div className="logo">
    //       <a href="/"> <img className="logo" src={logo} alt="logo" /></a>
    //     </div>
    //    <div className="menu">
    //         <a className="menu_text" href="/aboutus">AboutUs</a>
    //         <a className="menu_text" href="#services">Services</a>
    //         {/* <div className="products_menu"> */}
    //          <a className="menu_text" href="#product">Products</a>
    //          {/* <div className="submenu">
    //           <p>Buy Gems Online</p>
    //           <p>Buy Rudraksha Online</p>
    //           <p>Vastu</p>
    //           <p>Shivaling Idols</p> */}
    //         {/* </div>
    //         </div> */}
    //         <a className="menu_text" href="#blog">Blogs</a>
    //         <a className="menu_text" href="/contact">ContactUs</a>
    //     </div>
    //     <div className="search">
    //        
    //     </div>
    <Navbar className="Navbar" expand="lg">
      <Navbar.Brand href="/"><img className="logo" src={logo} alt="logo" /></Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav" >
        <Nav className="mr-auto">
          <Nav.Link href="/aboutus" className="menu_text">AboutUs</Nav.Link>
          <NavDropdown title="Services" id="basic-nav-dropdown" className="menu_text">
            {servicecatgory.map(category => {
              return (
                <div>
                  <NavDropdown.Item href={"/loveandrelationship/" + category.id} className="Item">{category.title}</NavDropdown.Item>
                  <NavDropdown.Divider />
                  {/* <NavDropdown.Item href="#action/3.2" className="Item">Vedic Astrology</NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item href="#action/3.3" className="Item">Astrology Reports 2020</NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item href="#action/3.4" className="Item">Bollywood Astrology</NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item href="#action/3.2" className="Item">Career Astrology</NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item href="#action/3.3" className="Item">Child Astrology</NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item href="#action/3.4" className="Item">Corporate Astrology</NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item href="#action/3.2" className="Item">Counselling</NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item href="#action/3.3" className="Item">Medical Astrology</NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item href="#action/3.4" className="Item">Numerology</NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item href="#action/3.4" className="Item">Rudraksha Therapy</NavDropdown.Item> */}
                </div>
              );
            })}
          </NavDropdown>
          <NavDropdown title="Products" id="basic-nav-dropdown" className="menu_text">
            {product.map(pro => {
              return (
                <div>
                  <NavDropdown.Item href={"/mainproduct/" + pro.id} className="Item">{pro.name}
                  </NavDropdown.Item>

                  <NavDropdown.Divider />
                </div>
              );
            })}
          </NavDropdown>
          <Nav.Link href="/blog" className="menu_text">Blogs</Nav.Link>
          <Nav.Link href="/contact" className="menu_text">ContactUs</Nav.Link>
        </Nav>
        <Form inline>
          <a className="search_text" href=""><img src={search} alt="search" /></a>
          <a className="search_text" href="/adminlogin"><img src={contact} alt="contact" /></a>
          <a className="search_text" href="/contact"><img src={location} alt="location" /></a>
        </Form>
      </Navbar.Collapse>
    </Navbar>
    // </div>
  );
}

export default NavBar;
