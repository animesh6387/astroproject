import React from 'react'
import '../scss/about.scss'
import '../scss/font.scss'
import '../scss/App.scss'
import '../scss/resets.scss'
import Navbar from './Navbar';
import Footer from './Footer';
import { Breadcrumb, Button } from 'react-bootstrap'
import '../scss/services.scss';
import '../scss/horoscope.scss';
import ico1 from '../images/horo/ico1.png'
import ico2 from '../images/horo/ico2.png'
import ico3 from '../images/horo/ico3.png'
import ico4 from '../images/horo/ico4.png'
import ico5 from '../images/horo/ico5.png'
import ico6 from '../images/horo/ico6.png'
import ico7 from '../images/horo/ico7.png'
import ico8 from '../images/horo/ico8.png'
import ico9 from '../images/horo/ico9.png'
import ico10 from '../images/horo/ico10.png'
import ico11 from '../images/horo/ico11.png'
import ico12 from '../images/horo/ico12.png'

const Libra = () => {
    return (
        <div className="AriesWrapper">
            <Navbar />
            <div className="header">
                <Breadcrumb className="bread">
                    <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
                    <Breadcrumb.Item active>Libra</Breadcrumb.Item>
                </Breadcrumb>
            </div>
            <div className="Aries">
                <div className="AriesLeft">
                    <Button variant="primary" className="btn">Daily</Button>{' '}
                    <Button variant="success" className="btn">Weekly</Button>{' '}
                    <Button variant="dark" className="btn">Monthly</Button>{' '}
                    <Button variant="danger" className="btn">Yearly</Button>
                    <div className="AriesHead">
                        <h1 className="AriesHeading">About Sunsign</h1>
                        <div className="AriesDesc">
                            <h4 className="head">Libra ( 23 September – 23 October )</h4>
                            <p className="AriesPara">Libra is the seventh sign of the zodiac. Libra is an active sign of the zodiac. The sign of the Libra is “balance”, and they are often found balancing themselves even in the most difficult situations. Libra traits suggest that they are two sided characteristics; one both cheerfulness and darkness are prominent in them. It is found that Libra born people hate to be alone. They keep themselves surrounded with the people. They are social people.Librans are intelligent and intuitive. They have a good communication skill, which makes them a good orator. Librans have charming personality and they often make them good host and hostesses.</p>
                            <p className="AriesPara">Libra is the sign of Air. They are very quick in changing their decision. Similarly, they are also unpredictable in their actions. They can be highly active sometimes and may become inactive sometimes. People born under this sign are great thinkers. They have inborn logical gift in them.</p>
                            <p className="AriesPara">The most important characteristics of Libra people are peace loving, ideal, superficial, pessimistic, elegant, welcoming and peace loving. Their negative quality is self – indulgence. Sometimes they become highly pessimistic towards life. They have the hardest time when they have to take crucial decision. The strength of Libra zodiac is positive attitude, diplomacy, grace, friendly, peace loving and grace. The main weaknesses of Librans are shallowness, unrealistic, futile and irresolute.Libra have great creative mind so they do well in the career of dramatist, artist, musician, sports figure, and novelist. They have acute sense of judgment. Libras make good lawyers and judges.</p>
                            <p className="AriesPara">Libras are ruled by the planet Venus. They are highly romantic at heart. They often settled for long term relationship and stay loyal to their partner. Libras take time in disclosing their feelings towards their partner. They are tender lovers. Libra women are sensual while Libras men are charming. Both of them can attract partners very easy. Lucky birthstones for Libra zodiac is Opal or Tourmaline. Their ruling planet is Venus. The lucky colors for Libra people are indigo, coffee, magenta, and lemon yellow. They may suffer from kidney problems, low back pain and bladder problems. The suitable career for this sign is art, music, writing and acting. The favorable months for them are January, February, March, July, September, and November.</p>
                        </div>
                    </div>
                </div>
                <div className="AriesRight">
                    <div className="RightDiv">
                        <a href="/Aries"> <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico1} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%', marginTop: '10px' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '30px' }}>Aries (Mar 21 - Apr 19)</p>
                        </div></a>
                        <hr />
                        <a href="/Taurus"> <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico2} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}> Taurus (Apr 20 - May 20)</p>
                        </div></a>
                        <hr />
                        <a href="/Gemini"> <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico3} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}>Gemini (May 21 - Jun 20)</p>
                        </div></a>
                        <hr />
                        <a href="/Cancer"> <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico4} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}>Cancer (Jun 21 - Jul 22)</p>
                        </div></a>
                        <hr />
                        <a href="/Leo"><div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico5} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}>Leo (Jul 23 - Aug 22)</p>
                        </div></a>
                        <hr />
                        <a href="/Virgo"><div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico6} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}>Virgo (Aug 23 - Sep 22)</p>
                        </div></a>
                        <hr />
                        <a href="/Libra"> <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico7} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}>Libra (Sep 23 - Oct 22)</p>
                        </div></a>
                        <hr />
                        <a href="/Scorpio"><div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico8} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}>Scorpio (Oct 23 - Nov 21)</p>
                        </div></a>
                        <hr />
                        <a href="/Sagittarius"> <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico9} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}> Sagittarius (Nov 22 - Dec 21)</p>
                        </div></a>
                        <hr />
                        <a href="/Capricorn"><div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico10} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}>Capricorn (Dec 22 - Jan 19)</p>
                        </div></a>
                        <hr />
                        <a href="/Aquarius"><div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <img src={ico11} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginTop: '10px' }}> Aquarius (Jan 20 - Feb 18)</p>
                        </div></a>
                        <hr />
                        <a href="/Pisces"> <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center', textAlign: 'center' }}>
                            <img src={ico12} alt="landr1" style={{ width: 50, height: 40, borderRadius: '50%', marginBottom: '30px' }} />
                            <p style={{ color: 'black', fontFamily: 'Minion Pro', fontWeight: 'bold', marginBottom: '30px', marginTop: '10px' }}>Pisces (Feb 19 - Mar 20)</p>
                        </div></a>
                    </div>

                </div>

            </div>
            <Footer />
        </div>
    );
}
export default Libra;