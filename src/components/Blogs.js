import React, { useState, useEffect } from "react";
import axios from 'axios';
import '../scss/App.scss';
import '../scss/font.scss';
import divider from '../images/Line 61.png';
import blog1 from '../images/blog.jpeg'
import blog2 from '../images/blog2.jpg'
import blog3 from '../images/blog3.jpg'


function Blogs() {
    const [blo, setBlo] = useState([])
    useEffect(() => {

        axios.get(' http://localhost:8080/api/latestBlog')

            .then(res => {
                console.log(res.data);
                setBlo(res.data.data);
            })
            .catch(err => {
                console.log(err);
            })
    })
    const getDate = (date) => {
        var options = { year: 'numeric', month: 'long', day: 'numeric' };
        return new Date(date).toLocaleDateString([], options);
    }
    return (
        <div className="Blogs" style={{ marginTop: '10%' }} >
            <p className="sale_text">LATEST BLOGS</p>
            <img className="divider" src={divider} alt="divider" />
            <div className="blogs_cart">
                {blo.map(blos => {
                    return (
                        <div className="expert_slider">

                            <div className="expert_image">
                                <img className="expert_cart_image" src={blos.image} alt="cart image" />
                            </div>
                            <div className="expert_cart_text">
                                <div className="expert_name">
                                    <p className="expert_text">{blos.title}</p>
                                    <p className="expert_text">{getDate(blos.date)}</p>
                                </div>
                            </div>

                        </div>
                    );
                })}


            </div>
        </div>
    );
}

export default Blogs;
